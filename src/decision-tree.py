#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 27 23:00:53 2018

@author: serenabonci
"""
#import numpy as np
#from sklearn import tree
import pandas as pd 
#import graphviz
from sklearn.externals.six import StringIO  
from IPython.display import Image  
from sklearn.tree import export_graphviz
import pydotplus

#takes in csv file, converts it to a list
df = pd.read_table("/Users/SerenaB/Desktop/SchoolShit/Data Mining/A3/gentrification.csv", delimiter=",")
mydata = df.values.tolist()

#df.columns.values gives names of columns
#print(data)

#training set

icecreamTraining=[
        ['Ben and Jerrys', 'Brown', 8, 'Chocolate'],
        ['Breyers', 'White', 2, 'Vanilla'],
        ['Haagen Dasz', 'Pink', 5, 'Strawberry'],
        ['Edies', 'Pink', 2, 'Cherry'],
        ['Ben and Jerrys', 'Brown', 7, 'Coffee'],
        ['Haagen Dasz', 'White', 6, 'Cookies and Cream'],
        ['Publix Brand', 'White', 3, 'Coconut'],
        ['Luigis Ice', 'Yellow', 6, 'Lemonade Sorbet'],
        ['Publix brand', 'Rainbow', 3, 'Artificial Bullshit'],
        ]

#column labels
header= ['Implementing Department', 'Action Category', 'Direct/Indirect', 'Targeted at Homelessness?', 'Actionable?']
#header= ['Brand', 'Colour', 'Hardness', 'Flavour']
def unique_vals(rows, col):
    return set([row[col] for row in rows])
unique_vals(mydata, 1)

'''finds the number of how many cases are actionable
and how many cases are not actionable'''
def col_instances(rows):
    instances={}
    for row in rows:
        label=row[-1]
        if label not in instances:
            instances[label]=0
        instances[label]+=1
    return instances


#col_instances(icecreamTraining)


'''checks whether a value is numeric or not, returns false
if the input is not a number'''
def numeric_checker(value):
    try:
        int(value)
        float(value)
        return True
    except ValueError:
        return False
    
numeric_checker(57)


class Question:
    def __init__(self, column, value):
        self.column=column
        self.value=value
    
    def match(self, example):
        val=example[self.column]
        if numeric_checker(val):
            return val >= self.value
        else:
            return val == self.value
        
    def __repr__(self):
        condition= '=='
        if numeric_checker(self.value):
            condition= '!='
        return 'Is %s %s %s?' %(
                header[self.column], condition, str(self.value))
#q=Question(1, 'White')
#q


# Let's pick an example from the training set...# Let's  
example = mydata[1]
# ... and see if it matches the question
q.match(example) # this will be true, since the first example is Green.
#######


def partition(rows, question):
    true_rows, false_rows= [], []
    for row in rows:
        if question.match(row):
            true_rows.append(row)
        else:
            false_rows.append(row)
    return true_rows, false_rows
#######
# Demo:
# Let's partition the training data based on whether rows are Red.
#true_rows, false_rows = partition(icecreamTraining, Question(1, 'White'))
# This will contain all the 'Red' rows.
#true_rows

#false_rows
def entropy(rows):
    instances=col_instances(rows)
    impurity=1
    for lbl in instances:
        prob_of_lbl=instances[lbl] / float(len(rows))
        impurity -= prob_of_lbl**2
    return impurity


def info_gain(left, right, current_uncertainty):
    p=float(len(left))/len(left)+len(right)
    return current_uncertainty - p * entropy(left) - (1-p) * entropy(right)


#current_uncertainty=entropy(icecreamTraining)
#true_rows, false_rows = partition(icecreamTraining, Question(0, 'Ben and Jerrys'))
#info_gain(true_rows, false_rows, current_uncertainty)


def find_best_split(rows):
    best_gain=0
    best_question=None
    current_uncertainty=entropy(rows)
    n_features=len(rows[0]) -1
    
    for col in range(n_features):
        values=set([row[col] for row in rows])
        
        for val in values:
            question=Question(col, val)
            true_rows, false_rows=partition(rows, question)
            if len(true_rows)==0 or len(false_rows)==0:
                continue
            gain=info_gain(true_rows, false_rows, current_uncertainty)
            if gain >= best_gain:
                best_gain, best_question = gain, question
        return best_gain, best_question


class Leaf:
    def __init__(self, rows):
        self.predictions=col_instances(rows)


class Decision_Node:
    def __init__(self,
                 question,
                 true_branch,
                 false_branch):
        self.question=question
        self.true_branch=true_branch
        self.false_branch=false_branch
        


def build_tree(rows):
        gain, question=find_best_split(rows)
        if gain==0:
            return Leaf(rows)
        true_rows, false_rows=partition(rows, question)
        true_branch=build_tree(true_rows)
        false_branch=build_tree(false_rows)
        
        return Decision_Node(question, true_branch, false_branch)
    
def print_tree(node, spacing=''):
        if isinstance(node, Leaf):
            print(spacing+'Predict', node.predictions)
            return
        print(spacing+str(node.question))
        
        print(spacing+'--> True:')
        print_tree(node.false_branch, spacing + ' ')
        
def classify(row, node):
        if isinstance(node, Leaf):
            return node.predictions
        if node.question.match(row):
            return classify(row, node.true_branch)
        else:
            return classify(row, node.false_branch)
        
def print_leaf(instances):
        total=sum(instances.values()) *1.0
        probs={}
        for lbl in instances.keys():
            probs[lbl]=str(int(instances[lbl]/total*100))+ '%'
        return probs
gentriTest= mydata
my_tree=build_tree(gentriTest)
classify(gentriTest[0], my_tree)


if __name__=='__main__':
        test_tree=build_tree(mydata)
        print_tree(test_tree)
        for row in mydata:
            print ('Actual: %s. Predicted: %s' %
                   (row[-1], print_leaf(classify(row,my_tree))))


print('Entropy:'), entropy(mydata)  


#dot_data = tree.export_graphviz(clf, out_file=None,  feature_names=features,  
#    class_names=mydata,  filled=True, rounded=True, special_characters=True) 
#graph = graphviz.Source(dot_data)  
#graph.render('dtree_render',view=True)

